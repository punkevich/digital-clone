from .DaughterServices import *


class MainService(BackProcessService,
                  ConsoleService,
                  UsersService,
                  VkApiService,
                  SQLService):
    def __init__(self):
        BackProcessService.__init__(self)
        ConsoleService.__init__(self)
        UsersService.__init__(self)
        VkApiService.__init__(self)
        SQLService.__init__(self)

    def run(self):
        """
        Запуск работы MainServices
        :return:
        """
        self.console.start()
        self.back.start()
