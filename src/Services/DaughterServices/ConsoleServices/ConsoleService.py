from threading import Thread

from .Console import *


class ConsoleService:
    def __init__(self):
        self.console = AdminConsole()


class AdminConsole:

    def __init__(self):
        self.console = Console(self)

        self.console_process = None

    def start(self):
        """
        Запуск консоли.
        :return:
        """
        self.console_process = Thread(name="Console", target=self.console.start, args=())
        self.console_process.start()

    def stop(self):
        """
        Остановка консоли
        :return:
        """
        self.console_process = None
