from threading import Thread
from queue import Queue
import os


class Console:

    def __init__(self, services):
        self.services = services

        self.user_name = "admin"

        os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

        self.commands_param_list = ["add-network", "remove-network", "start-train", "stop-train", "add-data",
                                    "is-training", "is-trained", "is-ready", "start-messaging"]

        self.commands_list = ["add-network", "remove-network", "start-train", "stop-train", "add-data",
                              "is-training", "is-trained", "is-ready", "start-messaging",
                              "networks-list", "networks-count", "help", "exit"]

        self.end_messaging_mark = "/exit"
        self.messaging = False
        self.messaging_network_name = None

        self.queue_print = Queue()

        self.t_input = None
        self.t_output = None

    def make_command(self, command: str):
        param = ""

        if " " in command:
            param = " ".join(command.split(" ")[1:])
            command = command.split(" ")[0]

        if param == "" and command in self.commands_param_list:
            self.print("<< This command needs parameter. Type /help to see commands list >>", end="\n\n>> ")
            return

        if " " in param and command != "add-data":
            self.print("<< Too many parameters >>", end="\n\n>> ")
            return

        if command == "messaging":
            if self.messaging_network_name is not None:
                if param == "/exit":
                    self.messaging = False

                    self.print("", end="\n==============================================\n")
                    self.print("<< Messaging have stopped >>", end="\n\n>> ")
                else:
                    try:
                        self.print(self.services.use_network(self.user_name, self.messaging_network_name, param),
                                   end="\n>> ")
                    except Exception as e:
                        self.print(str(e), end="\n\n>> ")
            else:
                self.print("<< Wat? How did you know about that command? >>", end="\n\n>> ")

        elif command in self.commands_list:
            if command == "add-network":
                if self.services.add_network(self.user_name, param):
                    self.print("<< Successfully added network {} >>".format(param), end="\n\n>> ")
                else:
                    self.print("<< Sorry, but network {} already exists >>".format(param), end="\n\n>> ")

            elif command == "remove-network":
                if param == "Han_Solo":
                    self.print("<< Permission denied >>", end="\n\n>> ")

                else:
                    try:
                        if self.services.remove_network(self.user_name, param):
                            self.print("<< Successfully removed network {} >>".format(param), end="\n\n>> ")

                        else:
                            self.print("<< Removing denied. Network {} is training >>".format(param), end="\n\n>> ")

                    except Exception as e:
                        self.print(str(e), end="\n\n>> ")

            elif command == "start-train":
                self.print("<< Starting network training >>")

                try:
                    if self.services.start_train(self.user_name, param):
                        self.print("<< Started training successfully >>", end="\n\n>> ")
                    else:
                        self.print("<< Current network is already training >>", end="\n\n>> ")
                except Exception as e:
                    self.print(str(e), end="\n\n>> ")

            elif command == "stop-train":
                self.print("<< Finishing network training >>")

                try:
                    if self.services.stop_train(self.user_name, param):
                        self.print("<< Stopped training successfully >>", end="\n\n>> ")
                    else:
                        self.print("<< {} network is already stopped >>".format(param), end="\n\n>> ")
                except Exception as e:
                    self.print(str(e), end="\n\n>> ")

            elif command == "is-training":
                try:
                    if self.services.is_training(self.user_name, param):
                        self.print("<< {} network is training >>".format(param), end="\n\n>> ")

                    else:
                        self.print("<< {} network is not training >>".format(param), end="\n\n>> ")

                except Exception as e:
                    self.print(str(e), end="\n\n>> ")

            elif command == "is-trained":
                try:
                    if self.services.is_trained(self.user_name, param):
                        self.print("<< {} network is trained >>".format(param), end="\n\n>> ")

                    else:
                        self.print("<< {} network is not trained >>".format(param), end="\n\n>> ")

                except Exception as e:
                    self.print(str(e), end="\n\n>> ")

            elif command == "is-ready":
                try:
                    if self.services.is_ready(self.user_name, param):
                        self.print("<< {} network is ready for training >>".format(param), end="\n\n>> ")

                    else:
                        self.print("<< {} network is not ready for training >>".format(param))
                        self.print("<< Please add data to this network >>")
                        self.print("<< To add data to network use /add-data command >>", end="\n\n>> ")

                except Exception as e:
                    self.print(str(e), end="\n\n>> ")

            elif command == "start-messaging":
                if self.services.is_trained(self.user_name, param):
                    self.messaging = True
                    self.messaging_network_name = param

                    self.print("<< Starting messaging with {} network >>".format(param),
                               end="\n==============================================\n>>")

                else:
                    if self.services.is_training(self.user_name, param):
                        self.print("<< Network is not trained, but training right now >>", end="\n\n>> ")
                    else:
                        self.print("<< Network is not trained >>")
                        self.print("<< You can start training network with /start-train command >>", end="\n\n>> ")

            elif command == "networks-list":
                self.print("<< Networks list >>")

                for name in self.services.get_networks_names(self.user_name):
                    self.print(" - " + name)
                self.print("", end="\n>> ")

            elif command == "networks-count":
                self.print("<< There are {} network(s) >>".format(self.services.get_networks_count(self.user_name)),
                           end="\n\n>> ")

            elif command == "add-data":
                if " " not in param:
                    self.print("<< This command needs more parameters >>", end="\n\n>> ")

                elif len(param.split(" ")) > 3:
                    self.print("<< Too much parameters >>", end="\n\n>> ")

                else:
                    param1 = param.split(" ")[1].split("=")
                    param2 = param.split(" ")[2].split("=")

                    if len(param1) != 2 or len(param2) != 2:
                        self.print("<< Typo in parameters >>", end="\n\n>> ")

                    else:
                        if param1[0] == "-i" and param2[0] == "-o":
                            src_file = param1[1]
                            tgt_file = param2[1]
                        elif param2[0] == "-i" and param1[0] == "-o":
                            src_file = param2[1]
                            tgt_file = param1[1]
                        else:
                            self.print("<< Typo in parameters >>", end="\n\n>> ")
                            return

                        dialogs = {}

                        try:
                            with open(src_file, "r") as src, \
                               open(tgt_file, "r") as tgt:
                                for src_line, tgt_line in zip(src.readlines(), tgt.readlines()):
                                        dialogs[src_line] = tgt_line

                        except FileNotFoundError:
                            self.print("<< Typo in file name(s) >>", end="\n\n>> ")

                        try:
                            self.services.add_network_data(self.user_name, param.split(" ")[0], dialogs)

                            self.print("<< Successfully added data to {} network >>".format(param.split(" ")[0]),
                                       end="\n\n>> ")

                        except Exception as e:
                            self.print(str(e), end="\n\n>> ")

            elif command == "help":
                self.print("<< List of commands >>")
                for cname in self.commands_list:
                    self.print("  " + cname)
                self.print("", end="\n>> ")

            elif command == "exit":
                self.print("<< Closing program >>")
                self.services.stop_all()

        else:
            self.print("<< Command {} not found. Please, type /help for commands list >>".format(command),
                       end="\n\n>> ")

    def print(self, s, end="\n"):
        self.queue_print.put(s + end)

    def _read(self):
        while True:
            s = input()
            if self.messaging:
                self.make_command("messaging " + s)
            else:
                self.make_command(s[1:])

    def _writing(self):
        while True:
            s = self.queue_print.get()
            print(s, end="")

    def start(self):
        self.t_input = Thread(name="console_input", target=self._read)
        self.t_output = Thread(name="console_output", target=self._writing)

        self.t_input.start()
        self.t_output.start()

        self.print("<< App started >>", end="\n\n>> ")

    def stop(self):
        pass
