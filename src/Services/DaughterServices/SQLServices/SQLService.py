from .SQL import *


class SQLService:

    def __init__(self):
        self.sql_db = SQLDB()

        self.sql_db.create_db()


class SQLDB:

    def __init__(self):
        self.sql = SQL()

    def create_db(self):
        return self.sql.create_db()

    def add_user(self, user_vk_id, user_auth_token, user_main_token=None):
        return self.sql.add_user(user_vk_id, user_auth_token, user_main_token)

    def add_network(self, user_vk_id, net_num: int, net_name: str, bot_token, train_st=False, work_st=False):
        return self.sql.add_network(user_vk_id, net_num, net_name, bot_token, train_st, work_st)

    def set_user_main_token(self, user_vk_id, user_main_token):
        return self.sql.set_user_main_token(user_vk_id, user_main_token)

    def set_network_training_state(self, user_vk_id, net_num: int, train_st: bool):
        return self.sql.set_network_training_state(user_vk_id, net_num, train_st)

    def set_network_working_state(self, user_vk_id, net_num: int, work_st: bool):
        return self.sql.set_network_working_state(user_vk_id, net_num, work_st)

    def set_network_name(self, user_vk_id, net_num: int, net_name: str):
        return self.sql.set_network_name(user_vk_id, net_num, net_name)

    def set_network_bot_token(self, user_vk_id, net_num: int, bot_token):
        return self.sql.set_network_bot_token(user_vk_id, net_num, bot_token)

    def set_network_targets(self, user_vk_id, net_num: int, targets: list):
        return self.sql.set_network_targets(user_vk_id, net_num, targets)

    def get_user_auth_token(self, user_vk_id: str) -> str:
        return self.sql.get_user_auth_token(user_vk_id)

    def get_user_main_token(self, user_vk_id: str) -> str:
        return self.sql.get_user_main_token(user_vk_id)

    def get_networks(self, user_vk_id: str) -> list:
        return self.sql.get_networks(user_vk_id)

    def get_network_targets(self, user_vk_id: str, net_num: int) -> list:
        return self.sql.get_network_targets(user_vk_id, net_num)
