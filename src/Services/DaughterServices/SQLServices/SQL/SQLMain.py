import sqlite3


class SQL:

    def __init__(self):
        self._connection = sqlite3.connect("src/DB/digital_clone.db")
        self._cursor = self._connection.cursor()

    def create_db(self):
        try:
            self._cursor.executescript("""
            CREATE TABLE users (
                user_id INTEGER PRIMARY KEY UNIQUE,
                user_vk_id UNIQUE,
                user_auth_token,
                user_main_token
                );
                """)
            print("Table users created")
            self._connection.commit()
        except sqlite3.OperationalError:
            print("Table users already exists")

        try:
            self._cursor.executescript("""
                CREATE TABLE networks (
                    net_id INTEGER PRIMARY KEY UNIQUE,
                    user_id INTEGER REFERENCES users (user_id) UNIQUE,
                    net_num,
                    net_name,
                    bot_token,
                    train_st,
                    work_st
                    );
                    """)
            self._connection.commit()
            print("Table networks created")
        except sqlite3.OperationalError:
            print("Table networks already exists")

        try:
            self._cursor.executescript("""
                CREATE TABLE targets (
                    tgt_id INTEGER PRIMARY KEY UNIQUE,
                    net_id INTEGER REFERENCES networks (net_id) UNIQUE,
                    tgt_user_vk_id
                    );
                    """)
            self._connection.commit()
            print("Table targets created")
        except sqlite3.OperationalError:
            print("Table targets already exists")

    def add_user(self, user_vk_id, user_auth_token, user_main_token=None):
        self._cursor.execute("INSERT OR REPLACE INTO users (user_vk_id, user_auth_token, user_main_token) "
                             "VALUES((SELECT user_id FROM users WHERE user_vk_id IS ?), ?, ?)",
                             (user_vk_id, user_auth_token, user_main_token))
        self._connection.commit()

    def add_network(self, user_vk_id, net_num: int, net_name: str, bot_token, train_st=False, work_st=False):
        self._cursor.execute("INSERT INTO networks "
                             "VALUES((SELECT user_id FROM users "
                             "WHERE user_vk_id IS ?), ?, ?, ?, ?, ?)",
                             (user_vk_id, net_num, net_name, bot_token, train_st, work_st))
        self._connection.commit()

    def set_user_main_token(self, user_vk_id, user_main_token):
        self._cursor.execute("UPDATE users SET user_main_token=? "
                             "WHERE user_id IS ?",
                             (user_main_token, user_vk_id))
        self._connection.commit()

    def set_network_training_state(self, user_vk_id, net_num: int, train_st: bool):
        self._cursor.execute("UPDATE networks SET train_st=? "
                             "WHERE user_id IS (SELECT user_id FROM users "
                             "WHERE user_vk_id IS ?) AND net_num IS ?",
                             (train_st, user_vk_id, net_num))
        self._connection.commit()

    def set_network_working_state(self, user_vk_id, net_num: int, work_st: bool):
        self._cursor.execute("UPDATE networks SET work_st=? "
                             "WHERE user_id IS (SELECT user_id FROM users "
                             "WHERE user_vk_id IS ?) AND net_num IS ?",
                             (work_st, user_vk_id, net_num))
        self._connection.commit()

    def set_network_name(self, user_vk_id, net_num: int, net_name: str):
        self._cursor.execute("UPDATE networks SET net_name=? "
                             "WHERE user_id IS (SELECT user_id FROM users "
                             "WHERE user_vk_id IS ?) AND net_num IS ?",
                             (net_name, user_vk_id, net_num))
        self._connection.commit()

    def set_network_bot_token(self, user_vk_id, net_num: int, bot_token):
        self._cursor.execute("UPDATE networks SET bot_token=? "
                             "WHERE user_id IS (SELECT user_id FROM users "
                             "WHERE user_vk_id IS ?) AND net_num IS ?",
                             (bot_token, user_vk_id, net_num))
        self._connection.commit()

    def set_network_targets(self, user_vk_id, net_num: int, targets: list):
        self._cursor.execute("DELETE FROM targets "
                             "WHERE net_id IS (SELECT net_id FROM networks "
                             "WHERE user_id IS (SELECT user_id FROM users "
                             "WHERE user_vk_id IS ?) AND net_num IS ?)",
                             (user_vk_id, net_num))
        self._connection.commit()

        if len(targets) == 0:
            return

        values = []
        for target in targets:
            values.append((user_vk_id, net_num, target))

        self._cursor.executemany("INSERT INTO targets "
                                 "VALUES((SELECT net_id FROM networks "
                                 "WHERE user_id IS (SELECT user_id FROM users "
                                 "WHERE user_vk_id IS ?) AND net_num IS ?), ?)",
                                 values)
        self._connection.commit()

    def get_user_auth_token(self, user_vk_id: str) -> str:
        self._cursor.execute("SELECT user_auth_token FROM users "
                             "WHERE user_id_vk IS ?",
                             (user_vk_id,))

        return self._cursor.fetchone()[0]

    def get_user_main_token(self, user_vk_id: str) -> str:
        self._cursor.execute("SELECT user_main_token FROM users "
                             "WHERE user_id_vk IS ?",
                             (user_vk_id,))

        return self._cursor.fetchone()[0]

    def get_networks(self, user_vk_id: str) -> list:
        self._cursor.execute("SELECT net_num, net_name, bot_token, train_st, work_st FROM networks "
                             "WHERE user_id IS (SELECT user_id FROM users "
                             "WHERE user_vk_id IS ?)",
                             (user_vk_id,))

        networks = []

        for network in self._cursor.fetchall():
            networks.append({"net_num": network[0],
                             "net_name": network[1],
                             "bot_token": network[2],
                             "train_st": network[3],
                             "work_st": network[4]})

        return networks

    def get_network_targets(self, user_vk_id: str, net_num: int) -> list:
        self._cursor.execute("SELECT tgt_user_vk_id FROM targets "
                             "WHERE net_id IS (SELECT net_id FROM networks "
                             "WHERE user_id IS (SELECT user_id FROM users "
                             "WHERE user_vk_id IS ?) AND net_num IS ?)",
                             (user_vk_id, net_num))

        targets = []

        for target in self._cursor.fetchall():
            targets.append(target[0])

        return targets
