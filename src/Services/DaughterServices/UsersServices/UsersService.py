from .Users import *


class UsersService:

    def __init__(self):
        self.users_networks = UsersNetworks()


class UsersNetworks:

    def __init__(self):
        self.users = Users()

    def add_network(self, user_name: str, network_name: str) -> bool:
        """
        Function adds network network_name to user user_name.
        :param user_name: User name.
        :param network_name: NN name.
        :return: True if successful, else - False.
        """
        return self.users.add_network(user_name, network_name)

    def remove_network(self, user_name: str, network_name: str) -> bool:
        """
        Function removes network with given name.
        :param user_name: User name.
        :param network_name: NN name.
        :return: True if successful, else - False.
        """
        return self.users.remove_network(user_name, network_name)

    def start_train(self, user_name: str, network_name: str) -> bool:
        """
        Запуск обучения нейронной сети под именем name.
        :param user_name: User name.
        :param network_name: NN name.
        :return: True if successful, else - False.
        """
        return self.users.start_network(user_name, network_name)

    def stop_train(self, user_name: str, network_name: str) -> bool:
        """
        Остановка обучения нейронной сети под именем name.
        :param user_name: User name.
        :param network_name: NN name.
        :return: True if successful, else - False.
        """
        return self.users.stop_network(user_name, network_name)

    def use_network(self, user_name: str, network_name: str, seq: str) -> str:
        """
        Использование нейронной сети под именем name.
        :param user_name: User name.
        :param network_name: NN name.
        :param seq: Строка-запрос str.
        :return: Ответ нейронной сети на запрос str.
        """
        return self.users.seq2seq_network(user_name, network_name, seq)

    def add_network_data(self, user_name: str, network_name: str, data: dict) -> bool:
        """
        Добавление данных для обучения нейронной сети под именем name.
        :param user_name: User name.
        :param network_name: NN name.
        :param data: Данные для обучения dict.
        :return: True if successful, else - False.
        """
        return self.users.add_data(user_name, network_name, data)

    def remove_network_data(self, user_name: str, network_name: str) -> bool:
        """
        Удаление данных для обучения нейронной сети под именем name.
        :param user_name: User name.
        :param network_name: NN name.
        :return: True if successful, else - False.
        """
        return self.users.remove_data(user_name, network_name)

    def get_network(self, user_name: str, network_name: str) -> bool:
        """
        Возвращает объект нейронной сети под именем name.
        :param user_name: User name.
        :param network_name: NN name.
        :return: Объект нейронной сети NeuralNetwork.
        """
        return self.users.get_network(user_name, network_name)

    def get_user(self, user_name: str):
        """
        Возвращает объект пользователя под именем user_name.
        :param user_name: User name.
        :return: Объект пользователя NeuralNetworks.
        """
        return self.users.get_user(user_name)

    def get_networks_names(self, user_name: str) -> list:
        """
        Возвращает список имен существующих нейронных сетей.
        :param user_name: User name.
        :return: Список имен list.
        """
        return self.users.get_networks_names(user_name)

    def get_networks_count(self, user_name: str) -> int:
        """
        Возвращает количество существующих нейронных сетей.
        :return: Количество сетей int.
        """
        return self.users.get_networks_count(user_name)

    def get_users_names(self) -> list:
        """
        Возвращает список имен существующих нейронных сетей.
        :return: Список имен list.
        """
        return self.users.get_users_names()

    def get_users_count(self) -> int:
        """
        Возвращает количество существующих нейронных сетей.
        :return: Количество сетей int.
        """
        return self.users.get_users_count()

    def is_training(self, user_name: str, network_name: str) -> bool:
        """
        Проверка запущена ли в настоящий момент тренировка нейронной сети под именем name.
        :param user_name: User name.
        :param network_name: NN name.
        :return: True if training, else - False.
        """
        return self.users.is_training(user_name, network_name)

    def is_trained(self, user_name: str, network_name: str) -> bool:
        """
        Проверка обучена ли нейронная сеть под именем name.
        :param user_name: User name.
        :param network_name: NN name.
        :return: True if trained, else - False.
        """
        return self.users.is_trained(user_name, network_name)

    def is_ready(self, user_name: str, network_name: str) -> bool:
        """
        Проверка подготовлена ли нейронная сеть под именем name к обучению.
        :param user_name: User name.
        :param network_name: NN name.
        :return: True if ready, else - False.
        """
        return self.users.is_ready(user_name, network_name)
