import traceback
try:
    from .nmt import nmt_train
    from .nmt import nmt_infer
except:
    traceback.print_exc()
from multiprocessing import Process
import fnmatch
import os
import re


class NeuralNetworks:

    def __init__(self, user_name):
        self.user_name = user_name
        self._user_dir = os.path.dirname(os.path.realpath(__file__)) + "/users/{}/".format(self.user_name)

        self._networks = {}
        self._networks_count = 0

        self._read_existing_networks()

    def _read_existing_networks(self):
        """
        Function is looking for existing folders in users/user_name/ directory and adds it to the _networks dict.
        :return: 
        """
        for network_name in os.listdir(self._user_dir):
            self._networks[network_name] = NeuralNetwork(network_name, self._user_dir)

            self._networks_count += 1

    def _add_network_dir(self, network_name):
        """
        Function adds network folder to users/user_name/ and creates data/ and model/ folders.
        :param network_name: name of network which is also the name of network folder.
        :return: 
        """
        os.makedirs(self._user_dir + network_name + "/data/")
        os.makedirs(self._user_dir + network_name + "/model/")

    def _remove_network_dir(self, network_name):
        """
        Function removes network folder from users/user_name/.
        :param network_name: Name of network.
        :return:
        """
        self._remove_dir(self._user_dir + network_name)

    def _remove_dir(self, d):
        for path in (os.path.join(d, f) for f in os.listdir(d)):
            if os.path.isdir(path):
                self._remove_dir(path)

            else:
                os.unlink(path)

        os.rmdir(d)

    def add_network(self, network_name: str) -> bool:
        if network_name in self._networks.keys():
            return False
            
        else:
            self._add_network_dir(network_name)

            self._networks[network_name] = NeuralNetwork(network_name, self._user_dir)

            self._networks_count += 1

            return True

    def remove_network(self, network_name: str) -> bool:
        if network_name in self._networks.keys():
            if self._networks[network_name].is_training():
                return False

            else:
                self._remove_network_dir(network_name)

                del self._networks[network_name]

                self._networks_count -= 1

                return True

        else:
            raise Exception("<< Network with given name does not exist >>")

    def start_network(self, network_name: str) -> bool:
        if network_name in self._networks.keys():
            if self._networks[network_name].is_training():
                return False

            else:
                if self._networks[network_name].is_ready():
                    self._networks[network_name].start_train()

                    return True
                else:
                    exception_msg = "<< {} network is not ready for training >>".format(network_name) + \
                                     "<< Please add data to this network >>" + \
                                     "<< To add data to network use /add-data command >>"
                    raise Exception(exception_msg)

        else:
            raise Exception("<< Network with given name does not exist >>")

    def stop_network(self, network_name: str) -> bool:
        if network_name in self._networks.keys():
            if self._networks[network_name].is_training():
                self._networks[network_name].stop_train()

                return True

        else:
            raise Exception("<< Network with given name does not exist >>")

    def add_data(self, network_name: str, data: dict) -> bool:
        """
        Function sets datafiles from given dict for network with same name as the given one.
        :param network_name: Name of network, which datafiles you want to set.
        :param data: Data as dict object. Its keys are network input sequences and its values are network responses.
        :return: True if data have been set successfully, else - False.
        """
        if network_name in self._networks.keys():
            if self._networks[network_name].is_training():
                return False

            else:
                self._networks[network_name].add_data(data)

                return True

        else:
            raise Exception("<< Network with given name does not exist >>")

    def remove_data(self, network_name: str) -> bool:
        """
        Function removes datafiles from network with same name as the given one.
        :param network_name: Name of network, which datafiles you want to remove.
        :return: True if data have been removed successfully, else - False.
        """
        if network_name in self._networks.keys():
            if self._networks[network_name].is_training():
                return False

            else:
                self._networks[network_name].remove_data()

                return True

        else:
            raise Exception("<< Network with given name does not exist >>")

    def get_network(self, network_name: str):
        """
        Function returns network object with same name as the given one.
        :param network_name: Name of network, which object you want to get.
        :return: NN object.
        """
        if network_name in self._networks.keys():
            return self._networks[network_name]

        else:
            raise Exception("<< Network with given name does not exist >>")

    def seq2seq_network(self, network_name: str, seq_query: str) -> str:
        if network_name in self._networks.keys():
            return self._networks[network_name].seq2seq(seq_query)
        else:
            raise Exception("<< Network with given name does not exist >>")

    def is_training(self, network_name: str) -> bool:
        if network_name in self._networks.keys():
            return self._networks[network_name].is_training()

        else:
            raise Exception("<< Network with given name does not exist >>")

    def is_trained(self, network_name: str) -> bool:
        if network_name in self._networks.keys():
            return self._networks[network_name].is_trained()

        else:
            raise Exception("<< Network with given name does not exist >>")

    def is_ready(self, network_name: str) -> bool:
        if network_name in self._networks.keys():
            return self._networks[network_name].is_ready()

        else:
            raise Exception("<< Network with given name does not exist >>")

    def get_networks_names(self) -> list:
        return list(self._networks.keys())

    def get_networks_count(self) -> int:
        return self._networks_count


class NeuralNetwork:

    def __init__(self, name: str, user_dir: str, steps=100000, units=1024, layers=1):
        self.name = name
        self._user_dir = user_dir
        self._user_data_dir = self._user_dir + self.name + "/data/"
        self._user_model_dir = self._user_dir + self.name + "/model/"

        self.steps = steps
        self.units = units
        self.layers = layers

        self._train_process = None
        self._seq2seq_process = None

        self._training_state = False
        self._trained_state = self._check_for_ckpt()
        self._ready_state = self._check_for_data()

    def is_ready(self):
        self._ready_state = self._check_for_data()

        return self._ready_state

    def is_training(self) -> bool:
        return self._training_state

    def is_trained(self) -> bool:
        self._trained_state = self._check_for_ckpt()

        return self._trained_state

    def remove_data(self):
        for file in os.listdir(self._user_data_dir):
            os.remove(self._user_data_dir+file)

    def add_data(self, data: dict):
        """
        Function sets datafiles from given dict.
        :param data: Data as dict object. Its keys are network input sequences and its values are network responses.
        :return: 
        """
        # src_vocab_list = ["<UNK>", "<s>", "</s>"]
        # tgt_vocab_list = ["<UNK>", "<s>", "</s>"]

        src_vocab_list = []
        tgt_vocab_list = []

        if not self._maybe_create_datafiles():
            with open(self._user_data_dir+"vocab.src") as src_vocab_r:
                with open(self._user_data_dir+"vocab.tgt") as tgt_vocab_r:
                    for word in src_vocab_r.readlines():
                        if word not in src_vocab_list and word != '' and word != '\n':
                            src_vocab_list.append(word)
                    for word in tgt_vocab_r.readlines():
                        if word not in tgt_vocab_list and word != '' and word != '\n':
                            tgt_vocab_list.append(word)

        with open(self._user_data_dir+"vocab.src", "w") as src_vocab, \
                open(self._user_data_dir+"vocab.tgt", "w") as tgt_vocab, \
                open(self._user_data_dir+"train.src", "a") as src_train, \
                open(self._user_data_dir+"train.tgt", "a") as tgt_train, \
                open(self._user_data_dir+"test.src", "a") as src_test, \
                open(self._user_data_dir+"test.tgt", "a") as tgt_test, \
                open(self._user_data_dir+"dev.src", "a") as src_dev, \
                open(self._user_data_dir+"dev.tgt", "a") as tgt_dev:
                    for i, (src_seq, tgt_seq) in enumerate(data.items()):
                        src_seq = self._filter_data_seq(src_seq)
                        tgt_seq = self._filter_data_seq(tgt_seq)

                        for word in src_seq.split(" "):
                            if word not in src_vocab_list:
                                src_vocab_list.append(word)
                        for word in tgt_seq.split(" "):
                            if word not in tgt_vocab_list:
                                tgt_vocab_list.append(word)

                        if i % 100 == 0:
                            src_dev.write(src_seq)
                            tgt_dev.write(tgt_seq)
                        elif i % 100 == 1:
                            src_test.write(src_seq)
                            tgt_test.write(tgt_seq)
                        else:
                            src_train.write(src_seq)
                            tgt_train.write(tgt_seq)

                    for word in src_vocab_list:
                        if word != '' and word != '\n':
                            src_vocab.write(word+"\n")

                    for word in tgt_vocab_list:
                        if word != '' and word != '\n':
                            tgt_vocab.write(word+"\n")

    def _maybe_create_datafiles(self) -> bool:
        """
        Function creates datafiles if there is not exist at list one datafile in user data folder.
        :return: True if datafiles have been created, else - False.
        """
        should_exist_datafiles = ["train.src", "test.src", "dev.src", "vocab.src",
                                  "train.tgt", "test.tgt", "dev.tgt", "vocab.tgt"]

        create = not self._check_for_data()

        if create:
            for datafile in should_exist_datafiles:
                file = open(self._user_data_dir+datafile, "w")
                file.close()

        return create

    @staticmethod
    def _filter_data_seq(seq: str) -> str:
        seq = re.sub("\.", " . ", seq)
        seq = re.sub("!", " ! ", seq)
        seq = re.sub("\?", " ? ", seq)
        seq = re.sub(",", " , ", seq)
        seq = re.sub(";", " ; ", seq)
        seq = re.sub(":", " : ", seq)
        seq = re.sub(" +", " ", seq)
        seq = re.sub("\. \. \.", "...", seq)
        seq = re.sub("&", " &amp;", seq)
        seq = re.sub("'", " &apos;", seq)
        seq = re.sub('"', " &quot;", seq)
        seq = re.sub("\[", " &#91;", seq)
        seq = re.sub("]", " &#93;", seq)

        return seq

    @staticmethod
    def _unfilter_data_seq(seq: str) -> str:
        seq = re.sub(" &#93;", "]", seq)
        seq = re.sub(" &#91;", "[", seq)
        seq = re.sub(' &quot;', '"', seq)
        seq = re.sub(" &apos;", "'", seq)
        seq = re.sub(" &amp;", "&", seq)
        seq = re.sub("\.\.\.", ". . .", seq)
        seq = re.sub(" \.", ".", seq)
        seq = re.sub(" !", "!", seq)
        seq = re.sub(" \?", "?", seq)
        seq = re.sub(" ,", ",", seq)
        seq = re.sub(" ;", ";", seq)
        seq = re.sub(" :", ":", seq)
        seq = re.sub(" +", " ", seq)

        return seq

    def start_train(self):
        self._training_state = True

        self._train_process = Process(target=nmt_train.main_func, name=self.name+"_train",
                                      args=(self._user_dir + self.name + "/", self.steps, self.units, self.layers))

        self._train_process.start()

    def stop_train(self):
        self._train_process.terminate()
        self._train_process = None

        self._training_state = False
        self._trained_state = self._check_for_ckpt()

    def seq2seq(self, seq_input: str) -> str:
        """
        Function gets input sequence (str object),
        process it in neural network and returns its response or None in case of error.
        :param seq_input: neural network input sequence.
        :return: neural network response sequence.
        """
        self._nmt_query(self._filter_data_seq(seq_input))

        file = open(self._user_data_dir+"infer_output_file.txt", "w")
        file.close()

        self._seq2seq_process = Process(target=nmt_infer.main_func, name=self.name+"_infer",
                                        args=(self._user_dir + self.name + "/",))
        self._seq2seq_process.start()
        self._seq2seq_process.join()
        self._seq2seq_process.terminate()
        self._seq2seq_process = None

        return self._unfilter_data_seq(self._nmt_result())

    def _nmt_query(self, seq_input: str):
        with open(self._user_data_dir+"infer_input.txt", "w") as infer_input_file:
            infer_input_file.write(seq_input)

    def _nmt_result(self) -> str:
        with open(self._user_data_dir+"infer_output.txt") as infer_output_file:
            return infer_output_file.readline()

    def _check_for_ckpt(self) -> bool:
        """
        Function checks if there are checkpoints files in model folder.
        :return: True if there is at least one .ckpt file, else - False.
        """
        for fname in os.listdir(self._user_model_dir):
            if fnmatch.fnmatch(fname, "*.ckpt*"):
                return True

        return False

    def _check_for_data(self) -> bool:
        """
        Function checks if there are all datafiles in user data folder which are should exist.
        :return: True if all datafiles are exist, else - False.
        """
        should_exist_datafiles = ["train.src", "test.src", "dev.src", "vocab.src",
                                  "train.tgt", "test.tgt", "dev.tgt", "vocab.tgt"]
        existing_datafiles = os.listdir(self._user_data_dir)

        result = True

        for should_exist_file in should_exist_datafiles:
            if should_exist_file not in existing_datafiles:
                result = False

        return result
