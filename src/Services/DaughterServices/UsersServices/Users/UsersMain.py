from .nn_ctrl import NeuralNetworks
import os


class Users:

    def __init__(self):
        self._users_dir = os.path.dirname(os.path.realpath(__file__)) + "/users/"

        self._users = {}
        self._users_count = 0

        self._read_existing_users()

    def _read_existing_users(self):
        """
        Function is looking for existing folders in users/ directory and adds it to the _networks dict.
        :return:
        """
        for user_name in os.listdir(self._users_dir):
            self._users[user_name] = NeuralNetworks(user_name)

            self._users_count += 1

    def _add_user_dir(self, user_name: str):
        """
        Function adds user folder to users/ and creates data/ and model/ folders.
        :param user_name: name of user which is also the name of user folder.
        :return:
        """
        os.makedirs(self._users_dir + user_name + "/")

    def _remove_user_dir(self, user_name: str):
        """
        Function removes user folder from users/.
        :param user_name: Name of user.
        :return:
        """
        self._remove_dir(self._users_dir + user_name)

    def _remove_dir(self, d):
        for path in (os.path.join(d, f) for f in os.listdir(d)):
            if os.path.isdir(path):
                self._remove_dir(path)

            else:
                os.unlink(path)

        os.rmdir(d)

    def add_user(self, user_name: str) -> bool:
        if user_name in self._users.keys():
            return False

        else:
            self._add_user_dir(user_name)

            self._users[user_name] = NeuralNetworks(user_name)

            self._users_count += 1

            return True

    def remove_user(self, user_name: str) -> bool:
        if user_name in self._users.keys():
            for network_name in self._users[user_name].get_networks_names():
                if self._users[user_name].is_training(network_name):
                    return False

            else:
                self._remove_user_dir(user_name)

                del self._users[user_name]

                self._users_count -= 1

                return True

        else:
            raise Exception("<< User {} does not exist >>".format(user_name))

    def add_network(self, user_name: str, network_name: str) -> bool:
        if user_name in self._users.keys():
            return self._users[user_name].add_network(network_name)

        else:
            raise Exception("<< User {} does not exist >>".format(user_name))

    def remove_network(self, user_name: str, network_name: str) -> bool:
        if user_name in self._users.keys():
            return self._users[user_name].remove_network(network_name)

        else:
            raise Exception("<< User {} does not exist >>".format(user_name))

    def start_network(self, user_name: str, network_name: str) -> bool:
        if user_name in self._users.keys():
            return self._users[user_name].start_network(network_name)

        else:
            raise Exception("<< User {} does not exist >>".format(user_name))

    def stop_network(self, user_name: str, network_name: str) -> bool:
        if user_name in self._users.keys():
            return self._users[user_name].stop_network(network_name)

        else:
            raise Exception("<< User {} does not exist >>".format(user_name))

    def add_data(self, user_name: str, network_name: str, data: dict) -> bool:
        if user_name in self._users.keys():
            return self._users[user_name].add_data(network_name, data)

        else:
            raise Exception("<< User {} does not exist >>".format(user_name))

    def remove_data(self, user_name: str, network_name: str) -> bool:
        if user_name in self._users.keys():
            return self._users[user_name].remove_data(network_name)

        else:
            raise Exception("<< User {} does not exist >>".format(user_name))

    def get_network(self, user_name: str, network_name: str):
        if user_name in self._users.keys():
            return self._users[user_name].get_network(network_name)

        else:
            raise Exception("<< User {} does not exist >>".format(user_name))

    def get_user(self, user_name: str):
        if user_name in self._users.keys():
            return self._users[user_name]

        else:
            raise Exception("<< User {} does not exist >>".format(user_name))

    def seq2seq_network(self, user_name: str, network_name: str, seq_query: str) -> str:
        if user_name in self._users.keys():
            return self._users[user_name].seq2seq_network(network_name, seq_query)

        else:
            raise Exception("<< User {} does not exist >>".format(user_name))

    def is_training(self, user_name: str, network_name: str) -> bool:
        if user_name in self._users.keys():
            return self._users[user_name].is_training(network_name)

        else:
            raise Exception("<< User {} does not exist >>".format(user_name))

    def is_trained(self, user_name: str, network_name: str) -> bool:
        if user_name in self._users.keys():
            return self._users[user_name].is_trained(network_name)

        else:
            raise Exception("<< User {} does not exist >>".format(user_name))

    def is_ready(self, user_name: str, network_name: str) -> bool:
        if user_name in self._users.keys():
            return self._users[user_name].is_ready(network_name)

        else:
            raise Exception("<< User {} does not exist >>".format(user_name))

    def get_networks_names(self, user_name) -> list:
        return list(self._users[user_name].get_networks_names())

    def get_networks_count(self, user_name) -> int:
        return self._users[user_name].get_networks_count()

    def get_users_names(self) -> list:
        return list(self._users.keys())

    def get_users_count(self) -> int:
        return self._users_count
