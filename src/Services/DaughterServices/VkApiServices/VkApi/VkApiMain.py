import vk_api
import re
import traceback


class VkApiAuth:

    def __init__(self, services):
        self.services = services

        from .....Main import main_http_address
        auth_page = main_http_address + "/vk/auth_auth"

        self.auth_session = self._auth_session()
        self.auth_defended_key = "1sdHKg4pRdSuuvyljkfC"
        self.vk_link_auth = "https://oauth.vk.com/authorize?client_id={}&" \
                            "display=page&redirect_uri={}&scope={}&response_type=token&v=5.80".format(
                                self.auth_session.app_id, auth_page, self.auth_session.scope)

    def get_users(self, users_id: str, field: str = "sex, bdate, city, country, home_town, has_photo,photo_200,online, "
                                                    "followers_count, occupation,nickname, relatives,relation"):
        users = {"users": self.auth_session.method(method="users.get", values={"user_ids": users_id, "fields": field})}
        return users

    def get_user(self, user_id: str, field: str = ""):
        user = self.auth_session.method(method="users.get", values={"user_ids": user_id, "fields": field})[0]
        return user

    def get_friends(self, user_id: str, field: str = "sex, photo_200_orig"):
        friends = self.auth_session.method(method="friends.get", values={"user_id": user_id, "fields": field})
        return friends

    def get_auth_token_from_url(self, url: str):
        pass  # TODO

    @staticmethod
    def _auth_session(token="84225357842253578422535778844710fb8842284225357df6d5cde8ddac058c00276f6") -> vk_api.VkApi:
        auth_session = vk_api.VkApi(
            token=token,
            app_id=6636460,
            scope=65539)
        return auth_session


class VkApiMain:

    def __init__(self, services):
        self.services = services

        self.main_session = self._main_session()
        self.main_defended_key = "Ibf4p3a92DwzrVSSs2Nk"
        self.vk_link_main = "https://oauth.vk.com/authorize?client_id={}&" \
                            "display=page&scope={}&response_type=token&v=5.80".format(
                                self.main_session.app_id, self.main_session.scope)

    @staticmethod
    def get_main_token_from_login(login: str, password: str):
        temp_session = vk_api.VkApi(
            token="c15dc657c15dc657c15dc657ebc1388510cc15dc15dc6579a133c3c9b68f4344bf1ce03",
            app_id=6636359,
            scope=69635,
            login=login,
            password=password)

        temp_session.auth()
        print(temp_session.token)
        return temp_session.token

    def get_main_token_from_url(self, url: str):
        pass  # TODO

    def get_messages_offset(self, user_id, token, offset=0):
        temp_session = self._main_session(token)

        messages = temp_session.method(method="messages.getHistory",
                                       values={"user_id": user_id, "count": 200, "offset": offset, "rev": 1})
        return messages

    def get_messages_all(self, user_id, token):
        of = 0
        last_user = True  # Последним сообщением было от токена
        first_mess = True
        first_enter = True

        s_in = ""
        s_out = ""

        while True:
            try:
                mess = self.get_messages_offset(user_id=user_id, token=token)
                print(mess)

                for i in mess["items"]:
                    if i["body"] == "":
                        continue

                    if i["user_id"] != i["from_id"] and first_mess:  # Проверка на то кто первый отправли сообщение
                        continue
                    elif i["user_id"] == i["from_id"] and first_mess:
                        first_mess = False

                    i["body"] = re.sub("\n", " ", i["body"])

                    if i["user_id"] == i["from_id"]:
                        if last_user is True:
                            if first_enter:
                                first_enter = False
                            else:
                                s_out += "\n"
                            last_user = False
                        s_in += i["body"] + " "
                    elif i["user_id"] != i["from_id"]:
                        if last_user is not True:
                            s_in += "\n"
                            last_user = True
                        s_out += i["body"] + " "

                    print(i["user_id"], "::", i["from_id"], "::", i["body"])
                of += 200

                if mess["items"] is []:
                    if s_out != "":
                        s_out += "\n"
                    break
            except:
                traceback.print_exc()
        return s_in, s_out

    def write_msg(self, user_id, token, s):
        temp_session = self._main_session(token)
        temp_session.method('messages.send', {'user_id': user_id, 'message': s})

    def get_message_now(self, token, values: dict):
        values.setdefault('out', 0)
        values.setdefault('count', 100)
        values.setdefault('time_offset', 60)

        temp_session = self._main_session(token)
        response = temp_session.method('messages.get', values)
        values['last_message_id'] = response['items'][0]['id']

        return response, values

    @staticmethod
    def _main_session(token="c15dc657c15dc657c15dc657ebc1388510cc15dc15dc6579a133c3c9b68f4344bf1ce03") -> vk_api.VkApi:
        main_session = vk_api.VkApi(
            token=token,
            app_id=6636359,
            scope=69635)
        return main_session

#
# def __down_wownik():
#     api_main = VkApiMain(None)
#     users = ["xolik", "santsantsant", "id350134490", "luciuscrow", "asmodeuscrow", "murielmerc"]
#
#     with io.open("fin.txt", mode="w", encoding="utf-8") as fin, \
#             io.open("fout.txt", mode="w", encoding="utf-8") as fout:
#         for user in users:
#             user_id = api_main.get_user(user_id=user, field="id")
#
#             s = api_main.get_messages_all(user_id, token="")
#
#             fin.write(s[0])
#             fout.write(s[1])
#
#
# # Выгружал свои диалоги. Коротко о том как это надо делать
# if __name__ == "__main__":
#     __down_wownik()
