from .VkApi import *


class VkApiService:

    def __init__(self):
        self.vk_api = Vk()


class Vk:

    def __init__(self):
        self.vk_auth = VkApiAuth(self)
        self.vk_main = VkApiMain(self)

    def get_users(self, users_id: str, field: str = "sex, bdate, city, country, home_town, has_photo,photo_200,online, "
                                                    "followers_count, occupation,nickname, relatives,relation"):
        return self.vk_auth.get_users(users_id, field)

    def get_user(self, user_id: str, field: str = ""):
        return self.vk_auth.get_user(user_id, field)

    def get_friends(self, user_id: str, field: str = "sex, photo_200_orig"):
        return self.vk_auth.get_friends(user_id, field)

    def get_messages_all(self, user_id, token):
        return self.vk_main.get_messages_all(user_id, token)

    def write_msg(self, user_id, token, s):
        self.vk_main.write_msg(user_id, token)

    def get_message_now(self, token, values: dict):
        return self.vk_main.get_message_now(token, values)
