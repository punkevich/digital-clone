from .Services import *
from .HTTPServer import *


class DependencyInjection:
    def __init__(self):
        self.main_service = MainService()
        self.http_server = HTTPServer()

    def run(self):
        """
        Запуск работы DependencyInjection.
        :return:
        """
        self.main_service.run()
        self.http_server.start_server()

    def stop(self):
        """
        Остановка работы DependencyInjection.
        :return:
        """
        self.main_service.stop_all()
        self.http_server.stop_server()


main_dependency_injection = None  # Костыль чтобы контроллеры знали о существовании сервисов. Дешево и сердито.
main_http_address = "localhost:8080"


def main():
    global main_dependency_injection
    main_dependency_injection = DependencyInjection()
    main_dependency_injection.run()
