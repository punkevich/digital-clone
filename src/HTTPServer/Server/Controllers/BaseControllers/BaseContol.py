import tornado.web


class BaseHandler(tornado.web.RequestHandler):
    """
    Базовые методы контроллеров.
    """

    def get_current_user(self):
        return {"access_token": self.get_secure_cookie("access_token"),
                "user_id": self.get_secure_cookie("user_id")}

    def authorizated(self):
        if self.get_current_user()["access_token"] is not None and \
                self.get_current_user()["user_id"] is not None:
            return True
        else:
            return False

    def get_services(self):
        from .....Main import main_dependency_injection
        return main_dependency_injection.main_service
