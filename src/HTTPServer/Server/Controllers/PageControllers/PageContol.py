# Огромный набор различных страниц

import tornado.web
from ..BaseControllers import *

from ...Web import *


class index(BaseHandler):
    """
    Страница index. Помянем.
    """

    def get(self):
        self.render(get_page_str("index.html"))


class MainHandler(BaseHandler):
    """
    Заглавная.
    """

    def get(self):
        self.render(get_page_str("MainHandler.html"))


class Profile(BaseHandler):
    """
    Заглавная.
    """

    def get(self):
        if self.authorizated():
            self.render(get_page_str("Profile.html"))
        else:
            self.redirect("/")


class BotPage(BaseHandler):
    """
    Заглавная.
    """

    def get(self):
        if self.authorizated():
            self.render(get_page_str("BotPage.html"))
        else:
            self.redirect("/")


class LogIn(BaseHandler):
    """
    Логининг. Помянем.
    """

    def get(self):
        self.render(get_page_str("LogIn.html"))

    def post(self):
        self.set_secure_cookie("login", self.get_argument("login"))
        self.set_secure_cookie("password", self.get_argument("password"))
        self.set_secure_cookie("access_token",
                               self.get_services().VkApi.get_main_token_from_login(login=self.get_argument("login"),
                                                                                   password=self.get_argument(
                                                                                       "password")))
        self.redirect("/kurwa")
