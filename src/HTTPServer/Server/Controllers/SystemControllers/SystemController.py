import tornado.web
from ..BaseControllers import *

from ...Web import *


class Kurwa(BaseHandler):
    """
    Вооооот.
    """

    def get(self):
        self.write(self.get_services().VkApi.vk_link_main + "\n")
        self.write(self.get_services().VkApi.vk_link_auth + "\n")
        self.write("Kurwa, login= {} : pass= {} : access_token= {} : expires_in= {} : user_id= {}".format(
            self.get_secure_cookie("login"),
            self.get_secure_cookie("password"),
            self.get_secure_cookie("access_token"),
            self.get_secure_cookie("expires_in"),
            self.get_secure_cookie("user_id")))


class ClearCookie(BaseHandler):
    """
    Страница, которая чистит твои куки
    """

    def get(self):
        self.clear_all_cookies()
        self.write("Cookie is cleared")


class ClearCookieH(BaseHandler):
    """
    Страница, которая чистит твои куки
    """

    def get(self):
        self.clear_all_cookies()
        self.redirect("/")
