import re
import json
from tornado import escape

from ..BaseControllers import *

from ...Web import *


class get_users(BaseHandler):
    """
    Страница, которая возвращает информанцию о текущем пользователе
    """

    def get(self):
        if not self.authorizated():
            self.write("not authorizated")
            return

        # j = escape.json_encode(self.get_services().VkApi.get_users(users_id=self.get_secure_cookie("user_id")))
        j = json.dumps(self.get_services().vk_api.get_users(users_id=self.get_secure_cookie("user_id")),
                       sort_keys=True,
                       indent=4)
        # j = re.sub("\n", "\r\n", j)

        self.write(j)
