import tornado.web
from ..BaseControllers import *

from ...Web import *


class auth_auth(BaseHandler):
    """
    Страница, которая аунтифицирует через вк для мейна
    """

    def get(self, h):
        if self.authorizated():
            self.redirect("/Profile")
            return

        if h == "a":
            self.redirect(self.get_services().vk_api.vk_auth.vk_link_auth)
            return
        elif h == "":
            self.render(get_page_str("url_anus.html"))
            return

        access_token = self.get_argument('access_token', None)
        expires_in = self.get_argument('expires_in', None)
        user_id = self.get_argument('user_id', None)

        self.write({"access_token": access_token,
                    "expires_in": expires_in,
                    "user_id": user_id})
        self.set_secure_cookie("access_token", access_token)
        self.set_secure_cookie("expires_in", expires_in)
        self.set_secure_cookie("user_id", user_id)

        self.get_services().sql_db.add_user(user_vk_id=user_id, user_auth_token=access_token)
        # self.get_services().users.add_user() TODO

        self.redirect("/Profile")
        return


class auth_main(BaseHandler):
    """
    Страница, которая аунтифицирует через вк для мейна
    """

    def get(self, h):
        if not self.authorizated():
            self.redirect("/")
            return

        if h == "a":
            self.redirect(self.get_services().vk_api.vk_main.vk_link_main)
            return
        elif h == "":
            self.render(get_page_str("url_anus.html"))
            return

        main_token = self.get_argument('access_token', None)
        expires_in = self.get_argument('expires_in', None)
        user_id = self.get_argument('user_id', None)

        self.write({"main_token": main_token,
                    "expires_in": expires_in,
                    "user_id": user_id})
        self.set_secure_cookie("main_token", main_token)
        self.set_secure_cookie("expires_in", expires_in)
        self.set_secure_cookie("user_id", user_id)

        self.get_services().sql_db.set_user_main_token(user_vk_id=user_id, user_main_token=main_token)

        return
