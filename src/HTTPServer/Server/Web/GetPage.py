import os


def get_page_str(page: str):
    """
    Возвращает путь до страниц
    :param page: Название страницы. Общепринятое в виде строки.
    :return: Строка=страница
    """
    page_way = os.path.dirname(__file__) + "/Pages/" + page
    return page_way
