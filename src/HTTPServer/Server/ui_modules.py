from tornado.web import UIModule


class Address(UIModule):
    def http_address(self):
        from ...Main import main_http_address
        return main_http_address  # {{% module http_address() %}}
