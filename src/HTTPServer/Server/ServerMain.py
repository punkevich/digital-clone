import os

import tornado.ioloop
import tornado.web
import tornado.httpserver

from . import ui_modules

# from .Web import *
from .Controllers import *


def vneh_ip():
    """
    Возвращает текущий ip сервера.
    Если сиё чудо будет повешено на хост, то надо будет запускать вместо локалхоста на этот ip.
    :return:
    """
    from requests import get
    ip = get('https://api.ipify.org').text
    return ip


class Server:
    """
    Класс, который собсно держит сайт. Ну или то что от него осталось.
    """

    @staticmethod
    def make_app():
        settings = {
            "static_path": os.path.dirname(__file__) + "/Web/Pages",
            "ui_modules": ui_modules
        }
        return tornado.web.Application([
            (r"/index", PageControllers.index),

            (r"/", PageControllers.MainHandler),
            (r"/Profile", PageControllers.Profile),
            (r"/BotPage", PageControllers.BotPage),
            (r"/log_in", PageControllers.LogIn),

            (r"/kurwa", SystemControllers.Kurwa),
            (r"/clear", SystemControllers.ClearCookie),
            (r"/clearh", SystemControllers.ClearCookieH),

            (r"/vk/auth_main(.*)", VKControllers.auth_main),
            (r"/vk/auth_auth(.*)", VKControllers.auth_auth),

            (r"/vk/get_users", VKControllers.get_users),
            (r"/vk/get_friends", VKControllers.get_friends),

            (r"/bot/AddBot", BotControllers.AddBot),
            (r"/bot/RemoveBot", BotControllers.RemoveBot),
            (r"/bot/ReloadBot", BotControllers.ReloadBot),
            (r"/bot/StartTrainBot", BotControllers.StartTrainBot),
            (r"/bot/StopTrainBot", BotControllers.StopTrainBot),
            (r"/bot/StartWorkBot", BotControllers.StartWorkBot),
            (r"/bot/StopWorkBot", BotControllers.StopWorkBot),
            (r"/bot/GetBotsList", BotControllers.GetBotsList),
            (r"/bot/GetBot", BotControllers.GetBot)
        ], **settings, cookie_secret="61oETzKXQAGaYdkL5gEmGeJJFuYh7EQnp2XdTP1o/Vo=")

    def __init__(self, services, ip="localhost", port=8080):
        """
        Создоние сайта
        :param ip: По дефолту локалхост. В идеале вообще надо писать внешний ip
        :param port: 8080- да будет вам известно, что это порт у сайтов.
        """
        self.services = services
        self.ip = ip
        self.port = port
        self.app = self.make_app()

    def start(self):
        """
        Слушает io через порт. Так же отчечает. Магия.
        :return:
        """
        print("[DigitalClone.tornado.web] Web services starting on {}: {}".format(self.ip, self.port))
        self.app.listen(self.port, self.ip)
        tornado.ioloop.IOLoop.current().start()

    def stop(self):
        tornado.ioloop.IOLoop.current().stop()
