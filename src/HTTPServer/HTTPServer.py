from .Server import *


class HTTPServer:
    def __init__(self):
        self.http_server = Server(self, "localhost", 8080)

    def start_server(self):
        """
        Запуск сервера.
        :return:
        """
        self.http_server.start()

    def stop_server(self):
        """
        Остановка сервера.
        :return:
        """
        self.http_server.stop()
